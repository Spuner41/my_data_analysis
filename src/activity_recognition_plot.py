from datetime import datetime, timedelta
from dateutil import tz

from utils import smutils
from utils.smutils import Constants as Const, Constants

# Plot para ver la actividad de varios días en Senior Monitoring
client = smutils.get_client()
users_pd = smutils.query_data(client=client, es_index=Const.USER_INDEX, es_type=Const.USER_TYPE,
                              fields=Const.USER_FIELDS)

users = list(users_pd.id)

start_date = datetime(2019, 3, 1, 0, 0, 0, tzinfo=tz.tzlocal())
for user in users:
    if user == "108466269235466101271":
        end_date = start_date + timedelta(days=1)
        activity_plot = smutils.full_report_plot(client, user, Constants.WIFI_SAMPLE_INDEX, from_date=start_date,
                                                 to_date=end_date, max_days=1)
        if activity_plot is not None:
            activity_plot.show()
