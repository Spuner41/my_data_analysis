from datetime import datetime, timedelta

from dateutil import tz

from utils import smutils
from utils.smutils import Constants

import matplotlib.pyplot as plt

import pandas as pd
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Plot / Timeline para visualizar en que momento se reciben o no las wifiSamples.
oscar = "108702230039451244550"
arturo = "105328954753520128293"
arturo_uji = "107262435143295313398"
guillermo = "114922491478536619253"
adrian = "103431461853105885299"
user = guillermo

################################################ Get users
# wifi = smutils.get_wifi_from_users()
# print("Hay un total de {} wifiSamples".format(len(wifi)))
# wifi = pd.DataFrame.from_dict(wifi)
# users = wifi['userId']
# users_list = []
# for user in users:
#     if user not in users_list:
#         users_list.append(user)
# print(users_list)
################################################

################################################ Get training
# wifi = smutils.get_wifi_train_from_rest(oscar)
# wifi = pd.DataFrame.from_dict(wifi)
# print(wifi.to_string())
################################################


start_date = datetime(2019, 10, 10, 0, 0, 0, tzinfo=tz.tzlocal())
end_date = start_date + timedelta(days=1)

#r = smutils.fog_report(user, start_date, end_date)
#print(r)

timeline = smutils.plot_wifi_timeline(user, start_date, end_date, 1)
timeline.show()

