from datetime import datetime, timedelta
from dateutil import tz
from utils import smutils
from utils.smutils import Constants

# Plot de UN día para FOG_SENIOR_MONITORING
client = smutils.get_client()
user = "105328954753520128293"

start_date = datetime(2019, 5, 1, 0, 0, 0, tzinfo=tz.tzlocal())
end_date = start_date + timedelta(days=1)

report_plot = smutils.fog_report(client, user, Constants.FOG_WIFI_SAMPLE_INDEX, from_date=start_date,
                                 to_date=end_date)
print(report_plot)

# classification_plot = smutils.simple_report_plot(client, user, Constants.FOG_WIFI_SAMPLE_INDEX, from_date=start_date,
#                                                  to_date=end_date)
# if classification_plot is not None:
#     classification_plot.show()
