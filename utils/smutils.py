from elasticsearch import Elasticsearch, RequestsHttpConnection
from datetime import timedelta, datetime
from dateutil import tz
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.dates import DateFormatter
import json
import pandas as pd
import numpy as np
from tqdm import tqdm
from sklearn.ensemble import RandomForestClassifier
import requests


def query_data(user, from_date, to_date):
    if from_date is not None and to_date is not None:
        from_tstmp = int(from_date.timestamp() * 1000)
        to_tstmp = int(to_date.timestamp() * 1000)

        return __build_location_dataframe(user, from_tstmp, to_tstmp)


def query_wifi(client, user, index, types, from_date=None, to_date=None):
    if 0 in types and 4 in types:
        print("ERROR: No se pueden obtener a la vez valores con tipos 0 y 4")
        exit()

    wifi_pd4 = None
    if 4 in types:
        types.remove(4)
        notfs_pd = query_data(client, Constants.NOTIFICATION_INDEX, Constants.NOTIFICATION_TYPE,
                              ['type', 'startTimestamp', 'endTimestamp', 'userResponse', 'validatedLocationId'],
                              from_date=from_date, to_date=to_date, user=user)

        if not notfs_pd.empty:
            notfs_pd = notfs_pd.loc[(notfs_pd.type == 2) & (notfs_pd.userResponse == 1), :]
            notfs_pd.drop(['type', 'userResponse'], axis=1, inplace=True)

            wifi_pd4 = query_wifi(client, user, [0], from_date=from_date, to_date=to_date)

            for _, row in notfs_pd.iterrows():
                start_timestamp = row['startTimestamp']
                end_timestamp = row['endTimestamp']
                location_id = row['validationNotificationId']
                wifi_pd4.loc[(wifi_pd4.timestamp >= start_timestamp) & (wifi_pd4.timestamp <= end_timestamp), [
                    'locationId']] = location_id
            wifi_pd4 = wifi_pd4[(wifi_pd4['locationId'] != 'None') & (wifi_pd4['locationId'] != '')]
            wifi_pd4['type'] = 4

    if len(types) > 0:
        query = {
            "query": {"bool": {"must": [{"bool": {"must": [{"match": {"userId": user}}], }}], }},
            "size": 100000
        }

        if from_date is not None and to_date is not None:
            from_tstmp = int(from_date.timestamp() * 1000)
            to_tstmp = int(to_date.timestamp() * 1000)
            query['query']['bool']['must'][0]['bool']['filter'] = \
                {"range": {"timestamp": {"gte": from_tstmp, "lte": to_tstmp}}}

        if len(types) > 0:
            query['query']['bool']['must'].append({'constant_score': {'filter': {'terms': {'type': types}}}})
        else:
            query['query']['bool']['must'].append({"match": {"type": 0}})

        body = json.dumps(query).encode('utf8')

        data = client.search(index=index, scroll='2m',
                             size=10000, body=body)

        total_hits = data['hits']['total']
        sid = data['_scroll_id']
        scroll_size = len(data['hits']['hits'])

        row_index = 0
        max_length = total_hits * 50
        data_dict = {'deviceId': np.zeros(shape=max_length, dtype='U10'),
                     'timestamp': np.zeros(shape=max_length, dtype=float),
                     'locationId': np.zeros(shape=max_length, dtype='U40'),
                     'type': np.zeros(shape=max_length, dtype=int)}

        while scroll_size > 0:
            for i in tqdm(range(scroll_size)):
                item = data['hits']['hits'][i]['_source']
                for scan in item['scanResultsList']:
                    for wap in scan:
                        if wap['BSSID'] not in data_dict.keys():
                            data_dict[wap['BSSID']] = np.zeros(shape=max_length, dtype=np.int32)
                        data_dict[wap['BSSID']][row_index] = wap['level']
                    data_dict['deviceId'][row_index] = item['deviceId']
                    data_dict['timestamp'][row_index] = item['timestamp']
                    data_dict['locationId'][row_index] = item['locationId']

                    row_index += 1

            data = client.scroll(scroll_id=sid, scroll='2m')
            sid = data['_scroll_id']
            scroll_size = len(data['hits']['hits'])

        wifi_pd = pd.DataFrame.from_dict(data_dict)
        wifi_pd.drop(wifi_pd.index[range(row_index, wifi_pd.shape[0])], inplace=True)

        if wifi_pd4 is not None:
            wifi_pd = pd.concat([wifi_pd, wifi_pd4], sort=False)

        return wifi_pd
    else:
        return wifi_pd4


def report(client, user, index, from_date=None, to_date=None, min_inactive_interval=20, min_ac_threshold=0.5):
    act_level_pd = query_data(client, es_index=Constants.ACTIVITY_LEVEL_INDEX, es_type=Constants.ACTIVITY_LEVEL_TYPE,
                              fields=Constants.ACTIVITY_LEVEL_FIELDS, user=user)
    if len(act_level_pd) == 0:
        print('El usuario {} no tiene niveles de actividad registrados.'.format(user))
        return None

    wifi_train = query_wifi(client, user, index, types=[1, 2, 3])
    wifi_unlabeled = query_wifi(client, user, index, types=[0], from_date=from_date, to_date=to_date)
    user_locations = query_data(client=client, es_index=Constants.LOCATION_INDEX, es_type=Constants.LOCATION_TYPE,
                                fields=['id', 'name'], user=user)

    y_tr = wifi_train['locationId']
    x_tr = wifi_train.drop(['deviceId', 'timestamp', 'type', 'locationId'], axis=1)
    x_tr = __scale_data(x_tr)

    rf_class = RandomForestClassifier(max_depth=20, n_estimators=100, max_leaf_nodes=50, random_state=42)
    rf_class.fit(x_tr, y_tr)

    # Descarta todos los puntos de acceso que no estén en el conjunto de entrenamiento
    bssid_list = list(x_tr)
    for bssid in bssid_list:
        if bssid not in wifi_unlabeled:
            wifi_unlabeled[bssid] = 0
    wifi_unlabeled = wifi_unlabeled[list(wifi_train)]

    user_alvls = act_level_pd.sort_values(by=['timestamp'])
    user_alvls.reset_index()
    start_ts = user_alvls['timestamp'].iloc[0]
    report_list = []

    for _, row in user_alvls.iterrows():
        if row['level'] > min_ac_threshold:
            interval = row['timestamp'] - start_ts
            if interval > min_inactive_interval * 60 * 1000:
                end_ts = row['timestamp']
                mask = (wifi_unlabeled.timestamp > start_ts) & (wifi_unlabeled.timestamp < end_ts)
                for bssid in bssid_list:
                    if bssid not in wifi_unlabeled:
                        wifi_unlabeled[bssid] = 0
                wifi_interval = wifi_unlabeled.loc[mask, bssid_list]
                timestamps = wifi_unlabeled.loc[mask, ['timestamp']]

                # Quita las muestras de fuera de casa
                not_home_mask = (wifi_interval.T != 0).any()
                wifi_interval = wifi_interval[not_home_mask]
                timestamps = timestamps[not_home_mask].values

                if len(wifi_interval) > 0:
                    wifi_interval.fillna(0, inplace=True)

                    # Scale data
                    wifi_interval = __scale_data(wifi_interval)

                    y_pr = rf_class.predict(wifi_interval)
                    y_pr = __majority_vote_by_timestamp(y_pr, timestamps)
                    if len(y_pr) > 0:
                        values, counts = np.unique(y_pr, return_counts=True)
                        pred_loc = values[np.argmax(counts)]
                        start_date = datetime.fromtimestamp(start_ts / 1000, tz.tzlocal())
                        end_date = datetime.fromtimestamp(end_ts / 1000, tz.tzlocal())
                        location_name = user_locations.loc[user_locations.id == pred_loc, 'name'].values[0]
                        report_list.append({'start_date': start_date, 'end_date': end_date, 'location': location_name})

            start_ts = row['timestamp']

    return report_list


def full_report_plot(client, user, index, from_date, to_date, max_days=8, min_inactive_interval=20,
                     min_ac_threshold=0.5,
                     title=None):
    reports_pd = []
    start_date = from_date
    actual_date = from_date
    for i in range(max_days):
        report_pd = report(client, user, index, start_date, to_date, min_inactive_interval, min_ac_threshold)
        print("Report {}/{} done!".format(i + 1, max_days))
        # Probar if not None
        reports_pd.append(report_pd)
        if i != max_days - 1:
            start_date = start_date + timedelta(days=1)
            to_date = start_date + timedelta(days=1)

    legend_lines = []
    legend_names = []
    fig, ax = plt.subplots(figsize=(10, 3))
    for i in range(max_days):
        for row in reports_pd[i]:
            if row['start_date'].day == row['end_date'].day == actual_date.day:
                plt.hlines(i, xmin=row['start_date'] - timedelta(days=i), xmax=row['end_date'] - timedelta(days=i),
                           color=Constants.COLORS[row['location']], alpha=0.5, linestyle='-',
                           linewidth=150 / len(reports_pd))
            else:
                if row['start_date'].day == actual_date.day:
                    ed = (row['start_date'] - timedelta(days=i)).replace(hour=23, minute=59)
                    plt.hlines(i, xmin=row['start_date'] - timedelta(days=i), xmax=ed,
                               color=Constants.COLORS[row['location']], alpha=0.5, linestyle='-',
                               linewidth=150 / len(reports_pd))
                elif row['end_date'].day == actual_date.day:
                    ed = (row['end_date'] - timedelta(days=i)).replace(hour=00, minute=00)
                    plt.hlines(i, xmin=ed, xmax=row['end_date'] - timedelta(days=i),
                               color=Constants.COLORS[row['location']], alpha=0.5, linestyle='-',
                               linewidth=150 / len(reports_pd))

            english_location = get_english_location(row['location'])

            if english_location not in legend_names:
                legend_names.append(english_location)
                legend_lines.append(Line2D([0], [0], color=Constants.COLORS[row['location']], lw=10, alpha=0.5))
        actual_date += timedelta(days=1)

    plt.xlabel('Hours')
    plt.ylabel('Days')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(True)
    ax.spines['left'].set_visible(True)
    ax.grid(alpha=0.25)
    plt.gca().xaxis.grid(True)
    ax.legend(legend_lines, legend_names, ncol=len(legend_names), loc=2, bbox_to_anchor=(0, 1.20), frameon=False)
    fig.tight_layout()
    if title is None:
        title = 'Range date: {} - {}'.format(from_date.strftime('%d/%m'), to_date.strftime('%d/%m'))
    plt.title(title, loc='right', fontsize=10, verticalalignment='baseline')

    return plt


def get_wifi_from_rest(user, start_date, end_date):
    start_date = int(start_date.timestamp() * 1000)
    end_date = int(end_date.timestamp() * 1000)
    url = "https://inti.init.uji.es:8453/getWifi/{}/{}/{}".format(user, start_date, end_date)
    with requests.Session() as session:
        session.verify = False
        session.headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36"}
        session.get(url)

        response = session.get(url, headers={"Accept": "application/json, text/javascript, */*; q=0.01",
                                             "X-Requested-With": "XMLHttpRequest",
                                             "Referer": "http://www.allflicks.net/",
                                             "Host": "www.allflicks.net"})
        # wifis = str(response.json()).replace("[", "")
        # wifis = wifis.replace("]", "")
        return response.json()


def get_wifi_train_from_rest(user):
    url = "https://inti.init.uji.es:8453/getTrainWifi/{}".format(user)
    with requests.Session() as session:
        session.verify = False
        session.headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36"}
        session.get(url)

        response = session.get(url, headers={"Accept": "application/json, text/javascript, */*; q=0.01",
                                             "X-Requested-With": "XMLHttpRequest",
                                             "Referer": "http://www.allflicks.net/",
                                             "Host": "www.allflicks.net"})
        return response.json()


def get_locations_from_rest(user):
    url = "https://inti.init.uji.es:8453/getLocations/{}".format(user)
    with requests.Session() as session:
        session.verify = False
        session.headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36"}
        session.get(url)

        response = session.get(url, headers={"Accept": "application/json, text/javascript, */*; q=0.01",
                                             "X-Requested-With": "XMLHttpRequest",
                                             "Referer": "http://www.allflicks.net/",
                                             "Host": "www.allflicks.net"})
        return response.json()


def get_wifi_from_users():
    url = "https://inti.init.uji.es:8453/getAllUsersWifi"
    with requests.Session() as session:
        session.verify = False
        session.headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36"}
        session.get(url)

        response = session.get(url, headers={"Accept": "application/json, text/javascript, */*; q=0.01",
                                             "X-Requested-With": "XMLHttpRequest",
                                             "Referer": "http://www.allflicks.net/",
                                             "Host": "www.allflicks.net"})
        return response.json()



def plot_wifi_timeline(user, start_date, end_date, days):
    wifi = get_wifi_from_rest(user, start_date, end_date)

    wifi = pd.DataFrame.from_dict(wifi)
    timestmaps = wifi['timestamp']
    dates = []

    counter = []
    for ts in timestmaps:
        dates.append(datetime.fromtimestamp(ts / 1000))

        if len(counter) == 0:
            counter.append(0)
        else:
            # counter.append(counter[len(counter) - 1] + 1)
            counter.append(0)

    dates.sort()

    plt.xlabel('Hours')
    plt.ylabel('Number of samples')
    plt.title("Wifi gathering path -> {}\n{}".format(user, start_date), loc='center', fontsize=10,
              verticalalignment='baseline')

    if days == 1:
        x_formatter = DateFormatter('%H:%M')
    else:
        x_formatter = DateFormatter('%d/%H')
    plt.gcf().axes[0].xaxis.set_major_formatter(x_formatter)

    plt.plot(dates, counter, 'o', markersize=2)

    return plt


def fog_report(user, from_date, to_date):
    wifi_train = get_wifi_train_from_rest(user)
    wifi_train = pd.DataFrame.from_dict(wifi_train)
    wifi_unlabeled = get_wifi_from_rest(user, from_date, to_date)
    wifi_unlabeled = pd.DataFrame.from_dict(wifi_unlabeled)
    user_locations = query_data(user, from_date, to_date)

    print(wifi_train)
    y_tr = wifi_train['locationId']

    x_tr = wifi_train.drop(['scanResultsList', 'userId', 'id', 'deviceId', 'timestamp', 'type', 'locationId'], axis=1)

    row_index = 0
    for scan in wifi_train['scanResultsList']:
        for wap in scan:
            if wap[0]['BSSID'] not in x_tr.keys():
                x_tr[wap[0]['BSSID']] = np.zeros(shape=len(wifi_train['scanResultsList'] * 50), dtype=np.int32)
            x_tr[wap[0]['BSSID']][row_index] = wap[0]['level']

            row_index += 1

    x_tr = __scale_data(x_tr)

    rf_class = RandomForestClassifier(max_depth=20, n_estimators=100, max_leaf_nodes=50, random_state=42)
    rf_class.fit(x_tr, y_tr)

    # Descarta todos los puntos de acceso que no estén en el conjunto de entrenamiento
    bssid_list = list(x_tr)
    mask = (wifi_unlabeled.timestamp > int(from_date.timestamp() * 1000)) & (
                wifi_unlabeled.timestamp < int(to_date.timestamp() * 1000))
    for bssid in bssid_list:
        if bssid not in wifi_unlabeled:
            wifi_unlabeled[bssid] = 0
    wifi_interval = wifi_unlabeled.loc[mask, bssid_list]
    timestamps = wifi_unlabeled.loc[mask, ['timestamp']]

    # Quita las muestras de fuera de casa
    not_home_mask = (wifi_interval.T != 0).any()
    wifi_interval = wifi_interval[not_home_mask]
    timestamps = timestamps[not_home_mask].values

    report_list = []

    # Sort
    start_ts = from_date  # pasar a milis?
    last = 0
    for wifi in wifi_unlabeled:
        wifi_interval = __scale_data(wifi_interval)
        print(wifi_interval)
        y_pr = rf_class.predict(wifi_interval)
        values, counts = np.unique(y_pr, return_counts=True)
        pred_loc = values[np.argmax(counts)]

        if last != pred_loc:
            end_ts = wifi['timestamp']
            start_date = datetime.fromtimestamp(start_ts / 1000, tz.tzlocal())
            end_date = datetime.fromtimestamp(end_ts / 1000, tz.tzlocal())
            report_list.append({'start_date': start_date, 'end_date': end_date, 'location': last})
            last = pred_loc
            start_ts = end_ts

    # for _, row in user_alvls.iterrows():
    #     if row['level'] > min_ac_threshold:
    #         interval = row['timestamp'] - start_ts
    #         if interval > min_inactive_interval * 60 * 1000:
    #             end_ts = row['timestamp']
    #             mask = (wifi_unlabeled.timestamp > start_ts) & (wifi_unlabeled.timestamp < end_ts)
    #             for bssid in bssid_list:
    #                 if bssid not in wifi_unlabeled:
    #                     wifi_unlabeled[bssid] = 0
    #             wifi_interval = wifi_unlabeled.loc[mask, bssid_list]
    #             timestamps = wifi_unlabeled.loc[mask, ['timestamp']]
    #
    #             # Quita las muestras de fuera de casa
    #             not_home_mask = (wifi_interval.T != 0).any()
    #             wifi_interval = wifi_interval[not_home_mask]
    #             timestamps = timestamps[not_home_mask].values
    #
    #             if len(wifi_interval) > 0:
    #                 wifi_interval.fillna(0, inplace=True)
    #
    #                 # Scale data
    #                 wifi_interval = __scale_data(wifi_interval)
    #
    #                 y_pr = rf_class.predict(wifi_interval)
    #                 y_pr = __majority_vote_by_timestamp(y_pr, timestamps)
    #                 if len(y_pr) > 0:
    #                     values, counts = np.unique(y_pr, return_counts=True)
    #                     pred_loc = values[np.argmax(counts)]
    #                     start_date = datetime.fromtimestamp(start_ts / 1000, tz.tzlocal())
    #                     end_date = datetime.fromtimestamp(end_ts / 1000, tz.tzlocal())
    #                     location_name = user_locations.loc[user_locations.id == pred_loc, 'name'].values[0]
    #                     report_list.append({'start_date': start_date, 'end_date': end_date, 'location': location_name})
    #
    #         start_ts = row['timestamp']

    return report_list


def get_english_location(es_location):
    if es_location == "Cocina":
        return "Kitchen"
    elif es_location == "Dormitorio":
        return "Bedroom"
    elif es_location == "Baño":
        return "Bathroom"
    elif es_location == "Despacho":
        return "Office"
    elif es_location == "Comedor":
        return "Dinning Room"
    elif es_location == "Salón":
        return "Living Room"


def __build_location_dataframe(user, from_tmstmp, to_tmstmp):
    fields = Constants.LOCATION_FIELDS
    data = get_locations_from_rest(user)
    total_hits = data['hits']['totalHits']
    df = pd.DataFrame(columns=fields, index=range(total_hits))
    psf = 0
    sid = data['scrollId']
    scroll_size = len(data['hits']['hits'])
    row_idx = 0

    # while scroll_size > 0:
    for i in tqdm(range(scroll_size)):
        item = data['hits']['hits'][i]['sourceAsMap']
        sr_dict = {}
        for field in fields:
            sr_dict[field] = item[field] if field in item else None
        df.loc[row_idx] = pd.Series(sr_dict)
        row_idx += 1

        # psf += len(data['hits']['hits'])
        # data = client.scroll(scroll_id=sid, scroll='2m')
        # sid = data['_scroll_id']
        # scroll_size = len(data['hits']['hits'])
    df.drop(df.index[range(row_idx, df.shape[0])], inplace=True)
    df.dropna(axis='columns', how='all', inplace=True)
    return df


def __scale_data(x):
    return x.applymap(lambda val: (((val + (val == 0) * -100) + 100) / 100))


def __majority_vote_by_timestamp(y, timestamps):
    if len(y) != len(timestamps):
        print("wrong timestamp length!")
        exit()
    new_y = []
    partial_y = []
    ts = timestamps[0]
    for i, timestamp in enumerate(timestamps):
        if ts == timestamp:
            partial_y.append(y[i])
        else:
            mv_pred = max(set(partial_y), key=partial_y.count)
            new_y.append(mv_pred)
            ts = timestamp
            partial_y = [y[i]]
    return np.array(new_y)


class Constants:
    USER_INDEX = "senior_monitoring"
    USER_TYPE = "users"
    USER_FIELDS = ['id', 'name', 'email']

    ACTIVITY_LEVEL_INDEX = "senior_activity_level"
    ACTIVITY_LEVEL_TYPE = "activity_level"
    ACTIVITY_LEVEL_FIELDS = ['userId', 'deviceId', 'timestamp', 'elapsedTime', 'level']

    LOCATION_INDEX = "senior_locations"
    LOCATION_TYPE = "locations"
    LOCATION_FIELDS = ['userId', 'id', 'deviceId', 'name', 'timestamp', 'configured', 'enabled', 'trainingTypes']
    FOG_LOCATION_INDEX = "fog_senior_locations"

    NOTIFICATION_INDEX = "senior_notifications"
    NOTIFICATION_TYPE = "notification"
    NOTIFICATION_FIELDS = ['userId', 'type', 'timestamp', 'startTimestamp', 'endTimestamp', 'userResponse',
                           'validatedLocationId']

    WIFI_SAMPLE_INDEX = "senior_wifi_samples"
    WIFI_SAMPLE_TYPE = "wifi_sample"
    FOG_WIFI_SAMPLE_INDEX = "fog_senior_wifi_samples"

    COLORS = {'Cocina': 'deepskyblue', 'Salón': 'lightcoral', 'Despacho': 'limegreen', 'Comedor': 'plum',
              'Baño': 'silver', 'Dormitorio': 'sandybrown'}
